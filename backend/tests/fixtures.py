import random
import string

import pytest

from auth9 import models as M
from auth9.web import app as the_app


def random_name() -> string:
    return ''.join(random.choice(string.ascii_letters + string.digits) for x in range(8))


@pytest.fixture
def app():
    the_app.config['DEBUG'] = False
    the_app.config['TESTING'] = True
    return the_app


@pytest.fixture
def user():
    password = random_name()
    new_user_name = random_name() + '@lab-m.ru'
    M.DBSession.begin()
    new_user = M.User(login=new_user_name, full_name=new_user_name)
    new_user.password = password
    M.DBSession.add(new_user)
    M.DBSession.commit()
    # We need the plaintext password later in tests, so set it just as a property
    new_user.plaintext = password
    return new_user
