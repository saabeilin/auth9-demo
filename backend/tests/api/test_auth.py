from auth9 import models as M

import pytest
from werkzeug.http import parse_cookie

from tests.fixtures import app, user, random_name


# No access to self details
@pytest.mark.xfail
def test_unauthorized(client):
    r = client.get('/api/whoami')
    assert r.status_code == 401
    assert r.headers['Content-Type'] == 'application/json'


# Login failures
def test_login_missing_params(client):
    r = client.post('/api/login')
    assert r.status_code == 400
    assert r.headers['Content-Type'] == 'application/json'


def test_login_nosuchuser(client):
    r = client.post('/api/login', data=dict(login='asd@asdfg.com', password='123123123'))
    assert r.status_code == 401
    assert r.headers['Content-Type'] == 'application/json'
    assert r.json['message_id'] == 'NO_SUCH_USER'


# Registration process
def test_register(client):
    """ Register new user
    """
    user_name_1 = random_name() + '@lab-m.ru'
    password = random_name()
    response = client.post('/api/register',
                           data={'login': user_name_1, 'password': password, 'password_repeat': password})
    assert response.status_code in (200, 201)
    assert response.json['login'] == user_name_1
    assert response.json['full_name'] == user_name_1

    user = M.DBSession.query(M.User).filter(M.User.login == user_name_1).one()
    assert user
    assert user.validate_password(password)


def test_register_alreadyexists(client, user):
    """ Fail to register existing user
    """
    response = client.post('/api/register',
                           data={'login': user.login, 'password': 'password', 'password_repeat': 'password'})
    assert response.status_code != 201
    assert response.json['message_id'] == 'ALREADY_REGISTERED'


def test_register_shortpassword(client):
    """ Fail to register with short password
    """
    user_name_1 = random_name() + '@lab-m.ru'
    response = client.post('/api/register',
                           data={'login': user_name_1, 'password': '123', 'password_repeat': '123'})
    assert response.status_code != 201
    assert response.json['message_id'] == 'BAD_PASSWORD'


def test_register_http_method(client):
    """ Test http method
    """
    response = client.get('/api/register', data={'user_name': 'irobot@lab-m.ru', 'user_password': '123123123'},
                          follow_redirects=True)
    assert response.status_code == 405  # 'method not allowed'


def test_register_invalid_email(client):
    """ Fail to register with invalid email
    """
    response = client.post('/api/register',
                           data={'login': "!@#$$%^&...", 'password': '123123123', 'password_repeat': '123123123'},
                           )
    assert response.status_code != 201
    assert response.json['message_id'] == 'INVALID_EMAIL'


@pytest.mark.xfail
def test_register_invalid_email_nxdomain(client):
    """ Fail to register with NX domain
    """
    response = client.post('/api/register',
                           data={'login': 'user@nonexistent.tld',
                                 'password': '123123123', 'password_repeat': '123123123'})
    assert response.status_code != 201
    assert response.json['message_id'] == 'INVALID_EMAIL'


def test_successlogin(client, user):
    """Test if we can login as an already registered user"""
    response = client.post('/api/login', data=dict(login=user.login, password=user.plaintext))
    assert response.status_code == 200
    assert response.json['login'] == user.login
    cookies_dict = {}
    cookies = response.headers.getlist('Set-Cookie')
    for cookie in cookies:
        cookies_dict.update(parse_cookie(cookie))
    assert 'session' in cookies_dict


def test_lastlogin(client, user):
    """Test the 'last login' endpoint"""

    # Login 10 times
    for _ in range(10):
        response = client.post('/api/login', data=dict(login=user.login, password=user.plaintext))
        assert response.status_code == 200

    response = client.get('/api/last_logins')
    assert response.status_code == 200
    assert len(response.json) == 5  # hardcoding requirements now
    assert all(u['user_id'] == user.id for u in response.json)


@pytest.mark.xfail
def test_cannot_login_twice(client, user):
    """Test if we can not login if already logged in"""
    response = client.post('/api/login', data=dict(login=user.login, password=user.plaintext))
    assert response.status_code == 200
    assert response.json['login'] == user.login
    cookies_dict = {}
    cookies = response.headers.getlist('Set-Cookie')
    for cookie in cookies:
        cookies_dict.update(parse_cookie(cookie))
    assert 'session' in cookies_dict
    raise NotImplementedError
