from auth9 import config
from sqlalchemy import DateTime
from sqlalchemy import create_engine
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import expression

engine = create_engine(config.DATABASE_URI)
maker = sessionmaker(autoflush=False, autocommit=True, expire_on_commit=False, bind=engine)
DBSession = scoped_session(maker)
session = DBSession

Base = declarative_base(bind=engine)


class utcnow(expression.FunctionElement):
    type = DateTime()


@compiles(utcnow, 'postgresql')
def pg_utcnow(element, compiler, **kw):
    return "TIMEZONE('utc', CURRENT_TIMESTAMP)"
