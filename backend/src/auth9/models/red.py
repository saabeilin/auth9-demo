import redis

from auth9 import config

SOCKET_TIMEOUT = 1
SOCKET_CONNECT_TIMEOUT = 0.5


redis_main = redis.StrictRedis.from_url(url=config.REDIS_URI, db=config.REDIS_DB_MAIN, decode_responses=True,
                                        socket_connect_timeout=SOCKET_CONNECT_TIMEOUT, socket_timeout=SOCKET_TIMEOUT)
redis_stats = redis.StrictRedis.from_url(url=config.REDIS_URI, db=config.REDIS_DB_STATS, decode_responses=True,
                                         socket_connect_timeout=SOCKET_CONNECT_TIMEOUT, socket_timeout=SOCKET_TIMEOUT)
redis_sesssion = redis.StrictRedis.from_url(url=config.REDIS_URI, db=config.REDIS_DB_SESSION,
                                            socket_connect_timeout=SOCKET_CONNECT_TIMEOUT,
                                            socket_timeout=SOCKET_TIMEOUT)
redis_auth = redis.StrictRedis.from_url(url=config.REDIS_URI, db=config.REDIS_DB_AUTH, decode_responses=True,
                                        socket_connect_timeout=SOCKET_CONNECT_TIMEOUT, socket_timeout=SOCKET_TIMEOUT)
