from _sha1 import sha1

import os
from sqlalchemy import Column, Integer, Unicode

from auth9.models.db import Base


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)

    login = Column(Unicode(128), index=True, nullable=False, unique=True)
    full_name = Column(Unicode(128), unique=False, index=True, nullable=False)

    _password = Column(Unicode(80))

    def _set_password(self, password):
        """Hash ``password`` on the fly and store its hashed version."""
        password_8bit = password.encode('UTF-8')

        salt = sha1()
        salt.update(os.urandom(60))
        hash = sha1()
        hash.update(password_8bit + salt.hexdigest().encode('UTF-8'))
        hashed_password = salt.hexdigest() + hash.hexdigest()

        # Make sure the hashed password is an UTF-8 object at the end of the
        # process because SQLAlchemy _wants_ a unicode object for Unicode
        # columns
        if not isinstance(hashed_password, str):
            hashed_password = hashed_password.decode('UTF-8')

        self._password = hashed_password

    def _get_password(self):
        """Return the hashed version of the password."""
        return self._password

    password = property(_get_password, _set_password)

    def validate_password(self, password):
        """
        Check the password against existing credentials.

        :param password: the password that was provided by the user to
            try and authenticate. This is the clear text version that we will
            need to match against the hashed one in the database.
        :type password: unicode object.
        :return: Whether the password is valid.
        :rtype: bool

        """
        if not self._password or not password:
            return False

        # if isinstance(password, unicode):
        password = password.encode('UTF-8')

        hashed_pass = sha1()
        hashed_pass.update(password + self.password[:40].encode('UTF-8'))
        return self.password[40:] == hashed_pass.hexdigest()

    @property
    def last_activity(self):
        return stats.get_user_last_activity(self.id)

    def __repr__(self):
        return f"<User id={self.id} login={self.login}>"
