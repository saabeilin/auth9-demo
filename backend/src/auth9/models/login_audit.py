import enum
import logging

from sqlalchemy import Column, Enum, Integer, ForeignKey, DateTime
from sqlalchemy.orm import relationship, backref

from auth9.models.db import Base, utcnow, DBSession
from auth9.models.user import User

logger = logging.getLogger(__name__)


class LoginStatus(enum.Enum):
    SUCCEEDED = 1
    FAILED = 0


class UserLoginAttempt(Base):
    __tablename__ = 'user_login_attempts'
    id = Column(Integer, primary_key=True)

    user_id = Column(Integer, ForeignKey(User.id))
    user = relationship(User, backref=backref('login_attempts', lazy='dynamic'))
    timestamp = Column(DateTime, index=True, nullable=False, server_default=utcnow())
    status = Column(Enum(LoginStatus), nullable=False)

    # TODO: add additional data: IP address, user-agent, etc.

    @property
    def json(self):
        return {
            'user_id': self.user_id,
            'timestamp': int(self.timestamp.timestamp() * 1000),
            'status': self.status.name
        }


def log_login_event(user: User, status: LoginStatus) -> None:
    """A helper to call from REST API etc"""
    DBSession.begin()
    login_attempt = UserLoginAttempt(user=user, status=status)
    DBSession.add(login_attempt)
    try:
        DBSession.commit()
    except:
        DBSession.rollback()
        logger.exception(f"Could not save login attempt information for user {user.id} ({status})")
