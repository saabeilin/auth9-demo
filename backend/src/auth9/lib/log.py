import logging.handlers

from flask import request

from . import auth


class AuthLoggingFilter(logging.Filter):
    """
    This is a filter which injects contextual information into the log.
    """

    def filter(self, record):
        # user = auth.current_user()
        # record.user_name = user.name if user else None
        record.user_id = auth.current_user_id()
        try:
            record.url = request.method + " " + request.url
        except:
            record.url = '(unknown)'
        try:
            record.user_agent = request.headers['User-Agent']
        except:
            record.user_agent = '(unknown)'

        return True
