import logging
from functools import wraps

from flask import g, request, abort, session

logger = logging.getLogger(__name__)


def prepare_auth_info():
    """Extracts auth information and saves it to `g` object"""
    g.user = g.user_id = None
    g.user_id = session.get('user_id', None)


def current_user():
    """
    @return: A User object, representing currently logged in user
    """
    if not request:
        return None

    if not hasattr(g, 'user'):
        return None

    if not g.user:
        if g.user_id:
            from ..models import DBSession, User
            g.user = DBSession.query(User).get(g.user_id)
            if not g.user:
                session['user_id'] = None

    return g.user


def current_user_id():
    """
    Required for passing as callable as 'default' value
    @return: the id of currently logged-in user.
    """
    try:
        return g.user_id
    except:
        return None


def auth_required(wrapped):
    """ Requires authentication, aborts with 401 otherwise """

    @wraps(wrapped)
    def decorated_function(*args, **kwargs):
        user = current_user()
        if not user:
            abort(401)
        request.user = user
        return wrapped(*args, **kwargs)

    return decorated_function
