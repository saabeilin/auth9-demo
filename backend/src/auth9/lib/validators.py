from validate_email import validate_email


def valid_email(email):
    try:
        return validate_email(email, check_mx=False, verify=False)
    except:
        pass
    return False
