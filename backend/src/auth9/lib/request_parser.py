"""
A patched version of RequestParser, not including omitted arguments
"""
from flask_restful import reqparse

__author__ = 'sbeilin'


class RequestParser(reqparse.RequestParser):
    def __init__(self, argument_class=reqparse.Argument, namespace_class=reqparse.Namespace,
                 trim=False, bundle_errors=False, store_missing=True):
        self.store_missing = store_missing
        super(RequestParser, self).__init__(
            argument_class=argument_class, namespace_class=namespace_class, trim=trim,
            bundle_errors=True)

    def add_argument(self, *args, **kwargs):
        """Adds an argument to be parsed"""

        # Only set it if it is not set explicitly
        if 'store_missing' not in kwargs:
            # if it is not required and has no default - SKIP IT!
            if not kwargs.get('required', False) and 'default' not in kwargs:
                kwargs['store_missing'] = self.store_missing

        return super(RequestParser, self).add_argument(*args, **kwargs)
