import logging

from envparse import Env
from logstash_formatter import LogstashFormatterV1

from auth9.lib.log import AuthLoggingFilter

env = Env()
# env.read_envfile()

DEBUG = env.bool('DEBUG', default=False)
TESTING = DEBUG

SECRET_KEY = 'dahruu0ki6quieb4zeR8Usohsee8odei'

if not DEBUG:
    SESSION_COOKIE_HTTPONLY = True
    SESSION_COOKIE_SECURE = True
    SESSION_COOKIE_DOMAIN = '.auth9.net'

    import rollbar

    ROLLBAR_TOKEN = env.str('ROLLBAR_TOKEN')
    ROLLBAR_ENV = 'production'
    rollbar.init(ROLLBAR_TOKEN, ROLLBAR_ENV)

DEFAULT_LOG_LEVEL = logging.DEBUG if DEBUG else logging.INFO

DATABASE_URI = env.str('DATABASE_URI')

REDIS_URI = env.str('REDIS_URI')
REDIS_DB_MAIN = 0
REDIS_DB_SESSION = 1
REDIS_DB_AUTH = 2
REDIS_DB_STATS = 8


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            '()': LogstashFormatterV1
        },
    },
    'handlers': {
        'console': {
            'level': DEFAULT_LOG_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
            'filters': ['auth_logging_filter'],
        },
    },
    'filters': {
        'auth_logging_filter': {
            '()': AuthLoggingFilter,
        }
    },
    'loggers': {
        # The default
        '': {
            'handlers': ['console'],
            'level': logging.INFO,
            'propagate': True,
        },
        # Some libraries used
        'flask': {
            'handlers': ['console'],
            'level': logging.INFO,
            'propagate': False,
        },
        'sqlalchemy': {
            'handlers': ['console'],
            'level': logging.INFO,
            'propagate': False
        }
    }
}


DD_STATSD_HOST = env.str('DD_STATSD_HOST', default='localhost')
