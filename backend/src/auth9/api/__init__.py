import logging

from flask_restful import Api as flask_restful_Api

from .auth import Login, Logout, Register
from .lastlog import LastLogin

endpoints = [
    Login, Logout, Register, LastLogin
]

logger = logging.getLogger(__name__)

__all__ = ['Api']


class Api(flask_restful_Api):
    def __init__(self, app=None, prefix=''):
        super(Api, self).__init__(app, prefix)
        self.mount()

    def unauthorized(self, response):
        """
        A hack to kill popping Auth dialogs
        """
        return response

    def mount(self):
        for resource_class in endpoints:
            try:
                mountpoint = (resource_class.mountpoint if hasattr(resource_class, 'mountpoint') else resource_class.__name__.lower())
                self.add_resource(resource_class,
                                  mountpoint,
                                  mountpoint + '/',
                                  mountpoint + '/<id>')
                logger.debug("Registered API %s", resource_class.__name__)
            except ValueError as e:
                logger.warning(e)
