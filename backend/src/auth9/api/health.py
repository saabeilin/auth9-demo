import json
import logging

from flask import Response
from flask_restful import Resource

from auth9 import models as M
from auth9.models import red

logger = logging.getLogger(__name__)

__all__ = ['health_check', 'Health']


def health():
    logger.info('Checking health')

    db_root = None
    try:
        db_root = M.DBSession.query(M.User).get(0)
    except:
        logger.exception('DB health: failed')

    red_dbsize = None
    try:
        red_dbsize = red.redis_main.dbsize()
    except:
        logger.exception('RDB health: failed')

    return {
        'database': db_root is not None,
        'redis': red_dbsize is not None,
    }


def health_check():
    return Response(json.dumps(health()), mimetype='application/json')


class Health(Resource):
    mountpoint = '.health'

    def get(self):
        return health()
