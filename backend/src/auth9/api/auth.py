# -*- coding: utf-8 -*-

from flask import current_app as app
from flask import session
from flask_restful import Resource, abort
from sqlalchemy.sql import func

from auth9 import models as M
from auth9.lib import validators
from auth9.lib.request_parser import RequestParser

__author__ = 'sbeilin'


def login_response(user: M.User) -> dict:
    """Prepares a result dictionary for login/register/google/facebook REST APIs"""
    return {
        'id': user.id,
        'login': user.login,
        'full_name': user.full_name,
    }


class Login(Resource):
    def post(self):
        parser = RequestParser() \
            .add_argument('login', type=str, required=True) \
            .add_argument('password', type=str, required=True)

        args = parser.parse_args()

        login = args['login']
        password = args['password']

        # User name, if given, must be a valid email
        if login and not validators.valid_email(login):
            app.logger.warning("Cannot log in a user with invalid email: '%s'" % login)
            abort(400, message="Please, provide a valid e-mail address",
                  message_id="INVALID_EMAIL")

        if not password or len(password) < 4:
            app.logger.warning("Cannot log in a user with invalid password: '%s'" % password)
            abort(400, message="Please, provide a better password",
                  message_id="BAD_PASSWORD")

        try:
            user = M.DBSession.query(M.User).filter(func.lower(M.User.login) == login.lower()).one()
        except Exception as e:
            app.logger.warning("User '%s' not found" % login)
            abort(401, message="No such user",
                  message_id="NO_SUCH_USER")

        if not user.validate_password(password):
            app.logger.warning("Wrong password for user '%s'" % login)
            M.log_login_event(user, M.LoginStatus.FAILED)
            abort(401, message="Wrong password",
                  message_id="WRONG_PASSWORD")

        session["user_id"] = user.id
        M.log_login_event(user, M.LoginStatus.SUCCEEDED)

        return login_response(user)


class Logout(Resource):
    def post(self):
        session["user_id"] = None
        return dict(status='success')


class Register(Resource):
    def post(self):
        parser = RequestParser() \
            .add_argument('login', type=str, required=True) \
            .add_argument('password', type=str, required=True) \
            .add_argument('password_repeat', type=str, required=True) \
            .add_argument('full_name', type=str)

        args = parser.parse_args()

        login = args['login']
        full_name = args['full_name']
        password = args['password']
        password_repeat = args['password_repeat']

        # User name, if given, must be a valid email
        if not login or login and not validators.valid_email(login):
            app.logger.warning("Cannot register a user with invalid email: '%s'" % login)
            abort(400, message="Please, provide a valid e-mail address",
                  message_id="INVALID_EMAIL")

        if not password or len(password) < 8:
            app.logger.warning("Cannot register a user with invalid password")
            abort(400, message="Please, provide a better password",
                  message_id="BAD_PASSWORD")

        if password != password_repeat:
            app.logger.warning("Passwords do not match")
            abort(400, message="Passwords do not match",
                  message_id="PASSWORDS_DO_NOT_MATCH")

        user = None
        try:
            user = M.DBSession.query(M.User).filter(func.lower(M.User.login) == login.lower()).one()
        except:
            user = None

        if user:  # Existing user!
            app.logger.warning("Cannot register a user '%s' - already exists" % login)
            abort(400, message="You are already registered!",
                  message_id="ALREADY_REGISTERED")

        user = M.User()
        try:
            M.DBSession.begin()

            user.login = login
            user.full_name = full_name or login
            user.password = password
            M.DBSession.add(user)
            M.DBSession.commit()
        except Exception as e:
            app.logger.exception(e)
            abort(500, message="Could not complete registration process. Please try again later or contact support.")

        session["user_id"] = user.id
        M.log_login_event(user, M.LoginStatus.SUCCEEDED)

        return login_response(user)
