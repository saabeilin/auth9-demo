from flask_restful import Resource

from auth9.models import UserLoginAttempt, LoginStatus
from auth9.lib.auth import current_user, auth_required

MAX_LAST_LOGINS = 5


class LastLogin(Resource):
    mountpoint = 'last_logins'

    @auth_required
    def get(self):
        user = current_user()
        attempts = user.login_attempts\
            .filter(UserLoginAttempt.status==LoginStatus.SUCCEEDED)\
            .order_by(UserLoginAttempt.timestamp.desc())\
            .limit(MAX_LAST_LOGINS)
        return [attempt.json for attempt in attempts]
