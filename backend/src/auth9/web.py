# -*- coding: utf-8 -*-
import logging.handlers
from logging.config import dictConfig

from flask import Flask, request
from flask_kvsession import KVSessionExtension
from simplekv.memory.redisstore import RedisStore
from werkzeug.contrib.fixers import ProxyFix

from auth9 import config
from auth9 import models as M
from auth9.api import Api
from auth9.api.health import health_check
from auth9.lib import auth
from auth9.models import red

app = Flask(__name__)
app.config.from_object(config)

simplekv_sess_store = RedisStore(red.redis_sesssion)
KVSessionExtension(simplekv_sess_store, app)

dictConfig(config.LOGGING)

logger = logging.getLogger(__name__)

logger.info("Booting %s" % __name__)


@app.before_request
def setup_session(*args, **kwargs):
    auth.prepare_auth_info()


@app.teardown_request
def shutdown_session(exception=None):
    M.DBSession.remove()


@app.after_request
def add_cors(resp):
    """
    Ensure all responses, including errors, have the CORS headers.
    This ensures any failures are also accessible by the client.
    """
    resp.headers['Access-Control-Allow-Origin'] = request.headers.get('Origin', '*')
    resp.headers['Access-Control-Allow-Credentials'] = 'true'
    resp.headers['Access-Control-Allow-Methods'] = 'POST, PUT, PATCH, DELETE, OPTIONS, GET'
    resp.headers['Access-Control-Allow-Headers'] = "X-Requested-With,X-Prototype-Version," \
                                                   "Content-Type,Cache-Control,Pragma,Origin,Cookie"
    resp.headers['Access-Control-Max-Age'] = '3600'
    return resp


app.add_url_rule('/.health', view_func=health_check)

API_ROOT = "/api/"
api = Api(app, API_ROOT)

app.wsgi_app = ProxyFix(app.wsgi_app)

logger.info("Ready %s" % __name__)

__all__ = [app]
