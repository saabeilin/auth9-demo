## Pre-requisites
 * docker engine, 17.06 or later
 * docker-compose

## Preparation
 * docker-compose pull && docker-compose build (or just `make build`)

## Running tests
 * `make tests` (this will also create the database; dev environ shares db instance with test)

## Running the project
 * `docker-compose up` or just `make run`
 * http://localhost:8080/


