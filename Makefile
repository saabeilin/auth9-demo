test:
	docker-compose run --rm api py.test --cov auth9 --no-cov-on-fail --cov-report term-missing  -v --no-print-logs  tests/

run:
	docker-compose up --abort-on-container-exit

ipython:
	docker-compose run --rm api ipython

build:
	docker-compose pull && docker-compose build
