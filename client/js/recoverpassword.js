/**
 * Created by sergei.beilin on 7/20/17.
 */

var auth9 = auth9 || {};
auth9.Auth = auth9.Auth || {};


auth9.Auth.RecoverPasswordRequestView = Marionette.View.extend({
  template: '#auth-recoverpassword-request-view-template',

  events: {
    'click #recoverpassword-request-button': 'sendButtonClicked',
  },

  sendButtonClicked: function (modal) {
    var email = $("#inputUserEmail").val();

    $.ajax({
      method: "POST",
      url: auth9.URLS.API_ROOT + "/recoverpassword",
      data: JSON.stringify({email: email}),
      dataType: 'json',
      xhrFields: {
        withCredentials: false
      },
      processData: false,
      contentType: 'application/json'
    }).done(function (data) {
      var modal = new Backbone.BootstrapModal({
        title: 'Password recovery',
        content: 'Password reset request has been sent.<br>Please, check Your mailbox for further instructions',
        animate: true,
        allowCancel: false
      }).open(function () {
      });
    }).fail(function (e) {
      var message = e.responseJSON.message;
      var modal = new Backbone.BootstrapModal({
        content: message,
        title: 'Could not reset password',
        animate: true,
        allowCancel: false
      }).open(function () {
      });
    });
  },

});
