/**
 * Created by sergei.beilin on 1/14/17.
 */

var auth9 = auth9 || {};
auth9.Auth = auth9.Auth || {};


auth9.Auth.GeoIpData = Backbone.Model.extend({
  url: 'https://freegeoip.net/json/',

  defaults: {
    'country_name': '',
    'country_code': '',
    'time_zone': 'UTC'
  }
});


auth9.Auth.Controller = Marionette.Object.extend({
  login: function () {
    app.showView(new auth9.Auth.AuthLoginView());
  },

  recoverPasswordRequest: function () {
    app.showView(new auth9.Auth.RecoverPasswordRequestView());
  },

  registerCompany: function () {
    app.showView(new auth9.Auth.AuthRegisterCompanyView());
  },

  showLastLoginsView: function () {
    console.log('showLastLoginsView');
    app.lastLogins = new auth9.Collections.LastLogins();
    app.lastLogins.fetch().done(function () {
      app.showView(new auth9.Views.LastLoginsTableView({collection: app.lastLogins}));
    })
  }
});


auth9.Auth.Router = Marionette.AppRouter.extend({
  appRoutes: {
    '': 'showLastLoginsView',
    'login': 'login',
    'recoverpassword': 'recoverPasswordRequest',
    'register': 'registerCompany'
  },

  controller: new auth9.Auth.Controller()
});


auth9.Auth.AuthApp = Backbone.Marionette.Application.extend({
  region: '#content',
  router: new auth9.Auth.Router(),
  geoIpData: new auth9.Auth.GeoIpData(),

  initialize: function (options) {
    console.log('Initialize');
  },

  loadData: function () {
    return this.geoIpData.fetch();
  },

  loadDataAndStart: function () {
    var theApp = this;
    this.loadData().then(function () {
      theApp.showLoadingOverlay(false);
      theApp.start();
    });
  },

  onStart: function () {
    Backbone.history.start();
  },

  showLoadingOverlay: function (show) {
    if (show) {
      $("#spinner-overlay").show();
    } else {
      $("#spinner-overlay").hide();
    }
  }
});


(function () {

  try {
    Rollbar.configure({
      payload: {
        client: {
          javascript: {
            source_map_enabled: true,
            code_version: AppVersion,
            environment: AppEnvironment,
            guess_uncaught_frames: true
          }
        }
      }
    })
  } catch (e) {
    console.log(e);
  }


  $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
    options.crossDomain = {
      crossDomain: true
    };

    if (!originalOptions.xhrFields) {
      options.xhrFields = {
        withCredentials: true
      };
    }
  });

  window.app = new auth9.Auth.AuthApp();

  $(function () {
    // moment.locale("en_GB");
    app.loadDataAndStart();
  });

})();
