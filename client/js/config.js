/**
 * Created by sbeilin on 2017-01-16.
 */

var auth9 = auth9 || {};

// auth9.API_HOST = "https://api.auth9.net";
auth9.API_HOST = "http://localhost:8000";

auth9.URLS = {
  API_HOST: auth9.API_HOST,
  API_ROOT: auth9.API_HOST + "/api"
};
