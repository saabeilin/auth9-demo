var auth9 = auth9 || {};
auth9.Views = auth9.Views || {};
auth9.Models = auth9.Models || {};
auth9.Collections = auth9.Collections || {};


auth9.Models.LastLogin = Backbone.Model.extend({
  defaults: {
    timestamp: null,
    status: null
  }
});

auth9.Collections.LastLogins = Backbone.Collection.extend({
  model: auth9.Models.LastLogin,
  url: auth9.URLS.API_ROOT + '/last_logins'
});


auth9.Views.LastLoginsTableRowView = Marionette.View.extend({
  tagName: "tr",
  className: "item-table-row",
  template: '#last-logins-table-row-template',

  events: {
    "click": "itemClicked"
  },

  modelEvents: {
    'change': 'render'
  }
});

auth9.Views.LastLoginsTableView = Marionette.CompositeView.extend({
  tagName: "table",
  className: "table table-striped table-condensed",
  childView: auth9.Views.LastLoginsTableRowView,
  template: "#last-logins-table-template"
});
