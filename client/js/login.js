/**
 * Created by sbeilin on 2017-01-14.
 */

var auth9 = auth9 || {};
auth9.Auth = auth9.Auth || {};


auth9.Auth.AuthLoginView = Marionette.View.extend({
  template: '#auth-login-view-template',

  events: {
    'click #login-button': 'loginButtonClicked'
  },

  loginButtonClicked: function (modal) {
    var login = $("#inputUsernameEmail").val();
    var password = $("#inputPassword").val();

    $.ajax({
      method: "POST",
      url: auth9.URLS.API_ROOT + "/login",
      data: JSON.stringify({login: login, password: password}),
      dataType: 'json',
      xhrFields: {
        withCredentials: true
      },
      processData: false,
      contentType: 'application/json'
    }).done(function (data) {
      Backbone.history.navigate('/', {trigger: true, replace: true})
    }).fail(function (e) {
      var message = e.responseJSON.message;
      var modal = new Backbone.BootstrapModal({
        content: message,
        title: 'Could not log you in',
        animate: true,
        allowCancel: false
      }).open(function () {
      });
    });
  }
});
