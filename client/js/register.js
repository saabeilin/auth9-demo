/**
 * Created by sergei.beilin on 1/14/17.
 */

var auth9 = auth9 || {};
auth9.Auth = auth9.Auth || {};


auth9.Auth.AuthRegisterCompanyView = Marionette.View.extend({
  template: '#auth-register-company-view-template',

  events: {
    'click #register-button': 'registerButtonClicked',
  },

  serializeData: function() {
    return _.clone(app.geoIpData.attributes);
  },

  getLang: function () {
    var locale;
    if (navigator.languages != undefined)
      locale = navigator.languages[0];
    else
      locale = navigator.language;

    return locale.replace('-', '_');
  },

  registerButtonClicked: function (modal) {
    var user_name = $("#register-user_name").val();
    var user_password = $("#register-user_password").val();
    var user_password_repeat = $("#register-user_password_repeat").val();
    var user_display_name = $("#register-user_display_name").val();

    if (!user_name || !user_password || !user_display_name) {
      var modal = new Backbone.BootstrapModal({
        content: "Sorry, all fields are required.",
        // title: 'Could not create an account',
        animate: true,
        allowCancel: false
      }).open(function () {
      });

      return;
    }

    if (user_password != user_password_repeat) {
      var modal = new Backbone.BootstrapModal({
        content: "Passwords do not match.",
        // title: 'Could not create an account',
        animate: true,
        allowCancel: false
      }).open(function () {
      });

      return;
    }

    $.ajax({
      method: "POST",
      url: auth9.URLS.API_ROOT + "/register",
      data: JSON.stringify({
        login: user_name,
        password: user_password,
        password_repeat: user_password_repeat,
        full_name: user_display_name
      }),
      dataType: 'json',
      xhrFields: {
        withCredentials: true
      },
      processData: false,
      contentType: 'application/json'
    }).done(function (data) {
      var modal = new Backbone.BootstrapModal({
        content: 'Welcome ' + data.full_name + ',<br>your account was successfully created!',
        title: 'Welcome!',
        animate: true,
        allowCancel: false
      }).open(function () {
      });

      Backbone.history.navigate('/', {trigger: true, replace: true})
      // window.location = '/';
    }).fail(function (e) {
      var message = e.responseJSON.message;
      var modal = new Backbone.BootstrapModal({
        content: message,
        title: 'Could not create an account',
        animate: true,
        allowCancel: false
      }).open(function () {
      });
    });
  }
});
